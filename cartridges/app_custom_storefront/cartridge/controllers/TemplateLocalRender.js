'use strict';

var server = require('server');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');

server.get('Show', cache.applyInventorySensitiveCache, function(req, res, next){
  res.render("Local");
  next();
});

module.exports = server.exports();