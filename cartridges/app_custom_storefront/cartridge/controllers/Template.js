'use strict';

var server = require('server');
var pageMetaData = require('*/cartridge/scripts/middleware/pageMetaData');
var cache = require('*/cartridge/scripts/middleware/cache');
var consentTracking = require('*/cartridge/scripts/middleware/consentTracking');


server.get('Show', function (req, res, next) {
  var a = [
    {
      nombre: 'conde'
    },
    {
      nombre: 'alexia'
    },
    {
      nombre: 'julio'
    },
    {
      nombre: 'shepe'
    },

  ];

  res.render("MyTemplateLocalRemote", { viewData: a });
  next();
});

module.exports = server.exports();